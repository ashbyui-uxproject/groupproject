package com.example.travelapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class addHotel extends AppCompatActivity {

    EditText confNumber, name, address, inDate, outDate, inTime, outTime;
    FileOutputStream fileOut;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hotel);
        confNumber = (EditText) findViewById(R.id.confNumEntry);
        name = (EditText) findViewById(R.id.hotelNameEntry);
        address = (EditText) findViewById(R.id.hotelAddressEntry);
        inDate = (EditText) findViewById(R.id.checkInDateEntry);
        outDate = (EditText) findViewById(R.id.checkOutDateEntry);
        inTime = (EditText) findViewById(R.id.checkInTimeEntry);
        outTime = (EditText) findViewById(R.id.checkOutTimeEntry);
        file = new File(getFilesDir() + "/hotels.csv");
        if (file.exists()){
            try {
                loadFile();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return;
        }
    }

    private void loadFile() throws IOException {
        FileInputStream fileIn = null;
        fileIn = openFileInput("hotels.csv");
        InputStreamReader streamReader = new InputStreamReader(fileIn);
        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder builder = new StringBuilder();
        String line;
        line = br.readLine();
        builder.append(line);
        name.setText(line, EditText.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        address.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        inDate.setText(line,TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        inTime.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        outDate.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        outTime.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        confNumber.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
    }


    public void sendHotel(View view) throws IOException {
        file.delete();
        Intent hotelData = new Intent(this, hotels.class);

        saveHotel();
        hotelData.putExtra("confNum", confNumber.getText().toString());
        hotelData.putExtra("name", name.getText().toString());
        hotelData.putExtra("address", address.getText().toString());
        hotelData.putExtra("inDate", inDate.getText().toString());
        hotelData.putExtra("outDate", outDate.getText().toString());
        hotelData.putExtra("inTime", inTime.getText().toString());
        hotelData.putExtra("outTime", outTime.getText().toString());
        startActivity(hotelData);
    }

    private void saveHotel() throws IOException {
        fileOut = null;
        fileOut = openFileOutput("hotels.csv", MODE_APPEND);
        fileOut.write(name.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(address.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(inDate.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(inTime.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(outDate.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(outTime.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(confNumber.getText().toString().getBytes());
    }
}