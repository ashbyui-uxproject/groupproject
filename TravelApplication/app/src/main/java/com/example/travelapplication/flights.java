package com.example.travelapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class flights extends AppCompatActivity {

    TextView flightName;
    TextView terminal;
    TextView arrivalTime;
    TextView departureTime;
    Intent extras;
    File flightsFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flights);

        extras = getIntent();
        flightName = (TextView) findViewById(R.id.flightName);
        terminal = (TextView) findViewById(R.id.flightTerminal);
        arrivalTime = (TextView) findViewById(R.id.arrival);
        departureTime = (TextView) findViewById(R.id.departure);

        try {
            loadFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        flightsFile = new File(getFilesDir()+"/flights.csv");
    }

    public void mainMenu(View view){
        Intent menu = new Intent(this, MainActivity.class);
        startActivity(menu);
    }

    private void loadFile() throws IOException {
        FileInputStream fileIn = null;
        fileIn = openFileInput("flights.csv");
        InputStreamReader streamReader = new InputStreamReader(fileIn);
        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder builder = new StringBuilder();
        String line;
        line = br.readLine();
        builder.append(line);
        flightName.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        line = br.readLine();
        builder.append(line);
        terminal.setText(builder.toString());
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        departureTime.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        arrivalTime.setText(line);
        builder.setLength(0);
    }

    public void deleteFlights(View view) throws IOException {
        flightsFile.delete();
        flightName.setText("");
        terminal.setText("");
        arrivalTime.setText("");
        departureTime.setText("");
    }

    public void updateFlights(View view) throws IOException {
        Intent addFlight = new Intent(this, addFlight.class);
        startActivity(addFlight);
    }
}