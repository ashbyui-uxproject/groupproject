package com.example.travelapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.*;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    File hotel;
    File flight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hotel= new File(getFilesDir() + "/hotels.csv");
        flight= new File(getFilesDir() + "/flights.csv");
       // hotel.delete();
        //flight.delete();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void addFlight(View view) {
        Intent addFlight = new Intent(this, addFlight.class);
        startActivity(addFlight);
    }

    public void viewFlight(View view){
        Intent viewFlight = new Intent(this,flights.class);
        startActivity(viewFlight);
    }

    public void viewHotel(View view){
        Intent viewHotel = new Intent(this, hotels.class);
        startActivity(viewHotel);
    }

    public void addHotel(View view) {
        Intent addHotel = new Intent(this, addHotel.class);
        startActivity(addHotel);
    }
}
