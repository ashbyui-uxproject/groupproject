package com.example.travelapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Time;


public class addFlight extends AppCompatActivity {

    EditText flightNumber;
    EditText terminal;
    EditText gate;
    TimePicker departure;
    TimePicker arrival;
    String flightTerminal;
    String depart;
    String arrive;
    FileOutputStream fileOut;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_flight);
        flightNumber = (EditText) findViewById(R.id.flightNumber);
        terminal = (EditText) findViewById(R.id.terminal);
        gate = (EditText) findViewById(R.id.gate);
        departure = (TimePicker) findViewById(R.id.departureTime);
        arrival = (TimePicker) findViewById(R.id.arrivalTime);
        departure.setIs24HourView(false);
        arrival.setIs24HourView(false);
        flightTerminal = terminal.getText().toString() + " " + gate.getText().toString();
        file = new File(getFilesDir() + "/flights.csv");
        if (file.exists()){
            try {
                loadFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return;
        }

    }

    private void loadFile() throws IOException {
        FileInputStream fileIn = null;
        fileIn = openFileInput("flights.csv");
        InputStreamReader streamReader = new InputStreamReader(fileIn);
        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder builder = new StringBuilder();
        String line;
        line = br.readLine();
        builder.append(line);
        flightNumber.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        terminal.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        gate.setText(line, TextView.BufferType.EDITABLE);
        builder.setLength(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void sendData(View view) throws IOException {
        Intent sendFlight = new Intent(this, flights.class);
        file.delete();
        depart = setTime(departure);
        arrive= setTime(arrival);
        flightTerminal = terminal.getText().toString() + " " + gate.getText().toString();
        saveFlight();
        sendFlight.putExtra("flightNum", flightNumber.getText().toString());
        sendFlight.putExtra("terminal", flightTerminal);
        sendFlight.putExtra("departureTime", depart);
        sendFlight.putExtra("arrivalTime", arrive);
        startActivity(sendFlight);
    }

    private void saveFlight() throws IOException {
        fileOut = null;
        fileOut = openFileOutput("flights.csv", MODE_APPEND);
        fileOut.write(flightNumber.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(terminal.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(gate.getText().toString().getBytes());
        fileOut.write(10);
        fileOut.write(depart.getBytes());
        fileOut.write(10);
        fileOut.write(arrive.getBytes());
        fileOut.write(10);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public String setTime(TimePicker time){
        time.setIs24HourView(true);
        int hour;
        String minute;
        String am_pm;

        if (time.getHour() < 12){
            am_pm = "AM";
        }
        else{
            am_pm = "PM";
        }
        hour = time.getHour();
        if (hour > 12){
            hour-=12;
        }
        else if (hour == 0){
            hour = 12;
        }

        if (time.getMinute() < 10){
            minute = "0"+time.getMinute();
        }
        else{
            minute = time.getMinute()+"";
        }

        return hour+":"+minute+" "+am_pm;
    }
}