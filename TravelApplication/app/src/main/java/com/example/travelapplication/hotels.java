package com.example.travelapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class hotels extends AppCompatActivity {

    TextView confNumber, name, address, inDate, outDate, inTime, outTime;
    Intent extras;
    File hotelsFile;
    FileInputStream fileIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotels);

        extras = getIntent();
        confNumber = (TextView) findViewById(R.id.confNumber);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        inDate = (TextView) findViewById(R.id.checkInDate);
        outDate = (TextView) findViewById(R.id.checkOutDate);
        inTime = (TextView) findViewById(R.id.checkInTime);
        outTime = (TextView) findViewById(R.id.checkOutTime);

        try {
            loadFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        hotelsFile = new File(getFilesDir()+"/hotels.csv");

    }

    public void mainMenu(View view){
        Intent menu = new Intent(this, MainActivity.class);
        startActivity(menu);
    }


    private void loadFile() throws IOException {
        FileInputStream fileIn = null;
        fileIn = openFileInput("hotels.csv");
        InputStreamReader streamReader = new InputStreamReader(fileIn);
        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder builder = new StringBuilder();
        String line;
        line = br.readLine();
        builder.append(line);
        name.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        address.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        inDate.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        inTime.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        outDate.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        outTime.setText(line);
        builder.setLength(0);
        line = br.readLine();
        builder.append(line);
        confNumber.setText(line);
        builder.setLength(0);
    }

    public void deleteHotels(View view) throws IOException {
        hotelsFile.delete();
        name.setText("");
        address.setText("");
        confNumber.setText("");
        inDate.setText("");
        outDate.setText("");
        inTime.setText("");
        outTime.setText("");
    }

    public void updateHotels(View view) throws IOException{
        Intent addHotel = new Intent(this, addHotel.class);
        startActivity(addHotel);
    }
}